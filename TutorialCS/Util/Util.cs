﻿using DayPilot.Web.Mvc.Recurrence;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TutorialCS.Models;

namespace TutorialCS.Util
{
    public class Util
    {
        public List<Employee> getEmployeesWithTimes(QueryEmployeeTimeIn.ActualEmployeeTimeByElementsResponseMessage_sync response, List<Employee> empList)
        {
            List<Employee> retList = new List<Employee>();
            if (response.ActualEmployeeTime != null)
            {
                foreach (var item in response.ActualEmployeeTime)
                {

                    if (!String.IsNullOrEmpty(item.EmployeeID.Value))
                    {
                        var buffer = empList.Where(s => s.EmployeeID.Equals(item.EmployeeID.Value));
                        foreach (var _employee in buffer)
                        {
                            retList.Add(new Employee(_employee.EmployeeID, _employee.FirstName, _employee.LastName, _employee.Department, _employee.DepartmentID));

                        }

                    }


                }

            }

            return retList;
        }

        public List<Employee> makeUniqueEmployees (List<Employee> retList)
        {
            var DistinctItems = retList.GroupBy(x => x.EmployeeID).Select(y => y.First()).ToList();
            retList = new List<Employee>();

            foreach (var item in DistinctItems)
            {
                retList.Add(item);
            }
            return retList;
        }
        public List<Employee> FillWithSAPEmployeeTimes(List<Employee> employees, QueryEmployeeTimeIn.ActualEmployeeTimeByElementsResponseMessage_sync response)
        {
            if(response.ActualEmployeeTime == null)
            {
                return employees;
            }
            var activeEmployeeTimes = response.ActualEmployeeTime.Where(time => time.LifeCycleStatusCode == QueryEmployeeTimeIn.EmployeeTimeItemLifeCycleStatusCode.Item1 
                || time.LifeCycleStatusCode == QueryEmployeeTimeIn.EmployeeTimeItemLifeCycleStatusCode.Item2 
                || time.LifeCycleStatusCode == QueryEmployeeTimeIn.EmployeeTimeItemLifeCycleStatusCode.Item3);
            Dictionary<string, Employee> employeeDict = new Dictionary<string, Employee>();
            foreach (Employee employee in employees)
            {
                string employeeID = employee.EmployeeID.TrimStart('0');
                employeeDict[employeeID] = employee;
            }
            List<string> absenceTypeCode = new List<string>(){
                "DE0029",   //Home office
                "DE0031",   //Vacation
                "DE0032",   //Vacation Unpaid
                "DE0035",   //Illness With Certificate
                "DE0036",   //Illness W/O Certificate (Paid)
                "DE0037",   //Illness Unpaid
                "DE0041",   //Maternity protection
                "DE0042",   //Parental time
                "DE0046",   //Unpaid leave
                "DE0053",   //Sabbatical
                "DE0069"    //Absent (unspecified)
            };
            foreach (var employeeTime in activeEmployeeTimes)
            {
                Employee employee = employeeDict[employeeTime.EmployeeID.Value];
                bool add = false;
                if(absenceTypeCode.Contains(employeeTime.ItemTypeCode.Value)){
                    employee.Events.Add(new AbsenceDay(employeeTime.UUID.Value, "Abwesenheit", employeeTime.DatePeriod.StartDate, employeeTime.DatePeriod.EndDate));
                    if (employeeTime.ProjectElementID != null)
                    {
                        employee.Projects.Add(new Project(employeeTime.ProjectElementID.Value.ToString(), "3"));
                    }
                }
            }
            return employees;
        }
        public List<Department> getDeparments(List<Employee> emplist)
        {
            List<Department> depList = new List<Department>();

            var DistinctItems = emplist.GroupBy(x => x.DepartmentID).Select(y => y.First()).ToList();
            foreach (var item in DistinctItems)
            {
                depList.Add(new Department(false,item.Department,item.DepartmentID));
            }
            return depList;

        }
        public bool checkDeps(string depname)
        {
            bool isPart = false;

            List<Department> deplist = HttpContext.Current.Session["DepList"] as List<Department>;

            var checkDepts = deplist.Where(m => m.CheckedValue == true) ;
            var test = checkDepts.Count();
            if (checkDepts.Count() == 0) { 
                isPart = true;
                
            }
            else
            {

                foreach (var item in checkDepts)
                {
                    if (item.Name==depname)
                    {
                        isPart = true;
                    }
                }

            }

            return isPart;
        }

        public bool checkProject(string Projectname)
        {
            bool isPart = false;

            List<Project> projectlist = HttpContext.Current.Session["ProjList"] as List<Project>;

            var checkProject = projectlist.Where(m => m.CheckedValue == true);
            var test = checkProject.Count();
            if (checkProject.Count() == 0)
            {
                isPart = true;

            }
            else
            {

                foreach (var item in checkProject)
                {
                    if (item.ProjectId + " "+item.ProjectName == Projectname)
                    {
                        isPart = true;
                    }
                }

            }

            return isPart;
        }

        public List<Project> getProjectList()
        {
            List<Project> _projectList = new List<Project>();

            DataAccess.DAL_BYD dataAccess = new DataAccess.DAL_BYD();
            _projectList = dataAccess.getProjectList();


            return _projectList;

        }


      
        
        public List<Project> makeUniqProject(List<Project> _projectList)
        {
            List<Project> uniqueProjectList = new List<Project>();

            

            var DistinctItems = _projectList.GroupBy(x => x.ProjectId).Select(y => y.First()).ToList();

            foreach (var project in DistinctItems)
            {
                uniqueProjectList.Add(project);
            }

            return uniqueProjectList;
        }


        public string ConvertToABAPTime(DateTime date)
        {
            string year = date.Year.ToString();
            string month = date.Month.ToString();

            if (month.Length<2)
            {
                month = "0" + month;
            }
                
            string day = date.Day.ToString();
            if (day.Length < 2)
            {
                day = "0" + day;
            }

            string retString = year + "-" + month + "-" + day;
            return retString;
        }


        public Project getProjectFromID(string selectedIndex)
        {

            Project retProject = new Project();
            string ProjectName = getNamefromProject(selectedIndex);
            retProject = getProjectFromName(ProjectName);
            return retProject;

        }
        public string getNamefromProject(string index)
        {
            List<SelectListItem> ProjectDropdownData = HttpContext.Current.Session["ProjectDropdownData"] as List<SelectListItem>;


            for (int i = 0; i < ProjectDropdownData.Count; i++)
            {
                if (ProjectDropdownData[i].Value.ToString() == index)
                {
                    return ProjectDropdownData[i].Text;
                }
            }
            return null;
        }
        public Project getProjectFromName(string ProjectName)
        {
            Project retProject = new Project();
            List<Project> projectList = HttpContext.Current.Session["ProjList"] as List<Project>;
            foreach (var item in projectList)
            {
                if (item.ProjectName == ProjectName)
                {
                    retProject = item;
                }
            }

            return retProject;
        }

        public string getEmpIDFromName(string empName)
        {
            List<Employee> empList = HttpContext.Current.Session["emplist"] as List<Employee>;
            foreach (var emp in empList)
            {
                string dispName = emp.FirstName + " " + emp.LastName;
                if (dispName == empName)
                {
                    return emp.EmployeeID;
                }
            }
            return "";
        }
        public string getEmpFromDropdown(string index)
        {
            List<SelectListItem> empDropdowndata = HttpContext.Current.Session["EmpDropdown"] as List<SelectListItem>;

            for (int i = 0; i < empDropdowndata.Count; i++)
            {
                if (empDropdowndata[i].Value.ToString() == index)
                {
                    return empDropdowndata[i].Text;
                }
            }
            return null;

        }
        public string getProjectNameFromProjectID(string ProjectID)
        {
            List<Project> projectList = HttpContext.Current.Session["ProjList"] as List<Project>;

            foreach (var project in projectList)
            {
                if (project.ProjectId == ProjectID)
                {
                    return project.ProjectId + " " + project.ProjectName;
                }
            }
            return null;
        }

        public string getProjectNameFromCreate(string index)
        {
            List<SelectListItem> ProjectDropdownData = HttpContext.Current.Session["CreateProjectDropdown"] as List<SelectListItem>;
            for (int i = 0; i < ProjectDropdownData.Count; i++)
            {
                if (ProjectDropdownData[i].Value.ToString() == index)
                {
                    string buffer = ProjectDropdownData[i].Text;
                    //string[] ret = buffer.Split(' ');
                    //buffer = "";
                    //for (int c = 1; c < ret.Length; c++)
                    //{
                    //    buffer = buffer + ret[c];
                    //}
                    return buffer;
                }
            }
            return null;
        }
        public string getProjectIDFromName(string ProjectName)
        {
            string projectID = ProjectName;
            string[] buffer = projectID.Split(' ');
            projectID = buffer[0];
            return projectID;
        }
        
        public string getRecType(int index) {
            List<string> recStrings = new List<string>();
            recStrings.Add("Täglich");
            recStrings.Add("Wöchentlich");
            recStrings.Add("Zwei-Wöchentlich");
            recStrings.Add("Monatlich");
            recStrings.Add("Jährlich");
            return recStrings[index];



        }

        public Employee getEmployeeByID(string id)
        {
            Employee retEmp = new Employee();
            List<Employee> empList = HttpContext.Current.Session["emplist"] as List<Employee>;
            foreach (var emp in empList)
            {
                if (emp.EmployeeID == id)
                {
                    retEmp = emp;
                }
            }

            return retEmp;
        }
        public int getTimesDaily(DateTime start, DateTime recEnd) {

            int times = 0;

            TimeSpan diff = recEnd - start;
            times = diff.Days;

            return times;

        }
        public int getTimesWeekly(DateTime start, DateTime recEnd)
        {
            int times = 0;

            TimeSpan diff = recEnd - start;
            times = diff.Days;
            times = times / 7;
            return times;

        }
        public int getTimesBiWeekly(DateTime start, DateTime recEnd)
        {
            int times = 0;
            LocalDate nStart = new LocalDate(start.Year, start.Month, start.Day);
            LocalDate nEnd = new LocalDate(recEnd.Year, recEnd.Month, recEnd.Day);

            Period period = Period.Between(nStart, nEnd, PeriodUnits.Months);
            times = (int) period.Months;
            return times;

        }
        public int getTimesMonthly(DateTime start, DateTime recEnd)
        {
            int times = 0;

            TimeSpan diff = recEnd - start;
            times = diff.Days;
            times = times / 14;
            return times;

        }

        public int getTimesYearly(DateTime start, DateTime recEnd)
        {
            int times = 0;            
            times = recEnd.Year -start.Year ;
            return times;

        }

        public RecurrenceRule createRecRule (string recType, DateTime start, DateTime recEnd, string id)
        {
            RecurrenceRule rule = null;
            int times = 0;
            switch (recType)
            {
                case "Täglich":
                     times = getTimesDaily(start, recEnd);
                     rule = RecurrenceRule.FromDateTime(id, start).Daily().Times(times);
                     break;
                case "Wöchentlich":
                     times = getTimesWeekly(start, recEnd);
                     rule = RecurrenceRule.FromDateTime(id, start).Weekly().Times(times);
  
                    //sstart = RecurrenceRule.DecodeStart(rule);
                  
                    
                     break;
                case "Zwei-Wöchentlich":
                    times = getTimesBiWeekly(start, recEnd);
                    DayOfWeek[] weekArray = new DayOfWeek[1];
                    weekArray[0] = start.DayOfWeek;
                    rule = RecurrenceRule.FromDateTime(id, start).Weekly(weekArray,2).Times(times);
                    break;
                case "Monatlich":
                    times = getTimesMonthly(start, recEnd);
                    rule = RecurrenceRule.FromDateTime(id, start).Monthly().Times(times);
                    break;
                case "Jährlich":
                    times = getTimesYearly(start, recEnd);
                    rule = RecurrenceRule.FromDateTime(id, start).Annually().Times(times);
                    break;
                default:
                    break;
            }
            return rule;
            
        }

        public RecurrenceRule createRecRule(string recType, DateTime start, int times, string id)
        {
            RecurrenceRule rule = null;

            switch (recType)
            {
                case "Täglich":
                  
                    rule = RecurrenceRule.FromDateTime(id, start).Daily().Times(times);
                    break;
                case "Wöchentlich":

                    rule = RecurrenceRule.FromDateTime(id, start).Weekly().Times(times);
                    break;
                case "Zwei-Wöchentlich":
                    DayOfWeek[] weekArray = new DayOfWeek[1];
                    weekArray[0] = start.DayOfWeek;
                    rule = RecurrenceRule.FromDateTime(id, start).Weekly(weekArray, 2).Times(times);
                    break;
                case "Monatlich":

                    rule = RecurrenceRule.FromDateTime(id, start).Monthly().Times(times);
                    break;
                case "Jährlich":
                    rule = RecurrenceRule.FromDateTime(id, start).Annually().Times(times);
                    break;
                default:
                    break;
            }
            return rule;

        }

        public RecurrenceRule createRecRuleUnlimited(string recType, DateTime start, string id)
        {
            RecurrenceRule rule = null;

            switch (recType)
            {
                case "Täglich":
                    rule = RecurrenceRule.FromDateTime(id, start).Daily().Indefinitely();

                    break;
                case "Wöchentlich":
                    rule = RecurrenceRule.FromDateTime(id, start).Weekly().Indefinitely();
                    break;
                case "Zwei-Wöchentlich":
                  
                    DayOfWeek[] weekArray = new DayOfWeek[1];
                    weekArray[0] = start.DayOfWeek;
                    rule = RecurrenceRule.FromDateTime(id, start).Weekly(weekArray, 2).Indefinitely();
                    break;
                case "Monatlich":
                    rule = RecurrenceRule.FromDateTime(id, start).Monthly().Indefinitely();
                    break;
                case "Jährlich":
                    rule = RecurrenceRule.FromDateTime(id, start).Annually().Indefinitely();
                    break;

                default:
                    break;
            }
            return rule;

        }
        public RecurrenceRule createRecRuleTimes(string recType, DateTime start,int times, string id)
        {
            RecurrenceRule rule = null;

            switch (recType)
            {
                case "Täglich":
                    rule = RecurrenceRule.FromDateTime(id, start).Daily().Times(times);

                    break;
                case "Wöchentlich":
                    rule = RecurrenceRule.FromDateTime(id, start).Weekly().Times(times);
                    break;
                case "Zwei-Wöchentlich":
                    DayOfWeek[] weekArray = new DayOfWeek[1];
                    weekArray[0] = start.DayOfWeek;
                    rule = RecurrenceRule.FromDateTime(id, start).Weekly(weekArray, 2).Times(times);
                    break;
                case "Monatlich":
                    rule = RecurrenceRule.FromDateTime(id, start).Monthly().Times(times);
                    break;
                case "Jährlich":
                    rule = RecurrenceRule.FromDateTime(id, start).Annually().Times(times);
                    break;
                default:
                    break;
            }
            return rule;

        }
        public DateTime getRecEndDate(string recType, DateTime start, int times)
        {
            
            DateTime endDate = new DateTime();

            switch (recType)
            {
                case "Täglich":

                    endDate = start.AddDays(times);
                    break;
                case "Wöchentlich":
                    endDate = start.AddDays(times*7);
                    break;
                case "Zwei-Wöchentlich":
                    endDate = start.AddDays(times * 14);
                  
                    break;
                case "Monatlich":
                    endDate = start.AddMonths(times);
                    break;
                case "Jährlich":
                    endDate = start.AddTicks(times);
                    break;
                default:
                    break;
            }
            return endDate;

        
    }





        public IList<SelectListItem> GetRecurringList()
        {
            IList<SelectListItem> recurringList = new List<SelectListItem>();
            List<string> recStrings = new List<string>();
            recStrings.Add("Täglich");
            recStrings.Add("Wöchentlich");
            recStrings.Add("Zwei-Wöchentlich");
            recStrings.Add("Monatlich");
            recStrings.Add("Jährlich");
            int i = 0;

            foreach (var recString in recStrings)
            {
                recurringList.Add(new SelectListItem { Text = recString, Value = i.ToString() });
                i++;
            }
            return recurringList;

        }
    }
}