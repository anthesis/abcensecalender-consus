﻿using DayPilot.Web.Mvc.Recurrence;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TutorialCS.Models;

namespace TutorialCS
{
    /// <summary>
    /// Summary description for EventManager
    /// </summary>
    public class EventManager
    {

        public DataTable BuildStandartTable()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("id");
            dt2.Columns.Add("name");
            dt2.Columns.Add("eventstart");
            dt2.Columns.Add("eventend");
            dt2.Columns.Add("resource");
            dt2.Columns.Add("tag");
            dt2.Columns.Add("recurrence");
            return dt2;
        }


        public DataTable FilteredData(DateTime start, DateTime end)
        {
            Util.Util util = new Util.Util();
            var employeeList = getList();
            List<Project> forFilter = HttpContext.Current.Session["ProjectFilter"] as List<Project>;


            int i = 0;
           
            DataTable dt2 = BuildStandartTable();

            foreach (var item in employeeList)
            {


                foreach (var item2 in item.Events)
                {
                    //TimeSpan diff = item2.End_Date - item2.Start_Date;
                    //int diffDays = diff.Days;
                    //endDate = item2.End_Date;

                    //if (diffDays > 0)
                    //{
                    //    end = item2.End_Date.AddDays(1);
                    //}
                    //else
                    //{
                       end = item2.End_Date;
                    //}

                    if (forFilter.Count != 0)
                    {
                        foreach (var project in forFilter)
                        {
                            if (project.ProjectId+" "+project.ProjectName == item2.Description)
                            {
                               
                                var row = dt2.NewRow();
                                row["id"] = item2.SAP_UUID;
                                row["name"] = project.ProjectName;
                                row["eventstart"] = item2.Start_Date;
                                row["eventend"] = end;
                                row["resource"] = item.EmployeeID;
                                row["tag"] = i;
                                if (item2.recRule != null)
                                {
                                    row["recurrence"] = item2.recRule.Encode();
                                }
                                dt2.Rows.Add(row);
                            }
                        }
                    }
                    else
                    {
                        if (item2.Description != null)
                        {
                            string id = "";
                            if (item2.Description =="Abwesenheit")
                            {
                                id = item2.Description;
                            }
                            else
                            {
                                
                                var projid = item2.Description.Split(' ');
                                for (int p = 1; p < projid.Length; p++)
                                {
                                    id = id + " " + projid[p];
                                }
                            }

                            var row = dt2.NewRow();
                            row["id"] = item2.EventID;
                            row["name"] = id;
                            row["eventstart"] = item2.Start_Date;
                            row["eventend"] = end;
                            row["resource"] = item.EmployeeID;
                            row["tag"] = i;
                            if (item2.recRule != null)
                            {
                                row["recurrence"] = item2.recRule.Encode();
                            }


                            dt2.Rows.Add(row);
                        }
                        

                        
                    }

                    
                }
                i++;

                
            }



            //da.Fill(dt);

            return dt2;
        }

        public void EventEdit(string id, string name, DateTime start, DateTime end, string resource)
        {
            var emplist = getList();

            foreach (var _employees in emplist)
            {
                if (_employees.EmployeeID == getIDByDisplayname(resource))
                {
                    foreach (var _event in _employees.Events)
                    {
                        if (_event.EventID == id)
                        {
                            _event.Description = name;
                            _event.Start_Date = start;
                            _event.End_Date = end;
                            break;
                        }
                    }
                }
            }
            saveList(emplist);
        }


        public DataTable GetResources()
        {
            bool isSet = false;
            Util.Util util = new Util.Util();
            List<Project> projectlist = HttpContext.Current.Session["ProjList"] as List<Project>;
            List<Project> forFilter = new List<Project>();
            if (projectlist!=null)
            {
                foreach (var item in projectlist)
                {
                    if (item.CheckedValue == true)
                    {
                        forFilter.Add(item);
                    }
                }
            }


            HttpContext.Current.Session["ProjectFilter"] = forFilter;
            var emplist = getList();

            DataTable dt = new DataTable();
            dt.Columns.Add("id");
            dt.Columns.Add("name");
            if (emplist!=null)
            {
                foreach (var item in emplist)
                {
                    if (forFilter.Count != 0)
                    {
                        isSet = false;

                        foreach (var project in item.Events)
                        {
                            foreach (var selectedProject in forFilter)
                            {
                                if (project.Description == selectedProject.ProjectId + " " + selectedProject.ProjectName)
                                {
                                    if (util.checkDeps(item.Department))
                                    {
                                        if (isSet == false)
                                        {
                                            var row = dt.NewRow();
                                            row["id"] = item.EmployeeID;
                                            row["name"] = item.FirstName + " " + item.LastName;
                                            dt.Rows.Add(row);
                                            isSet = true;
                                        }
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        if (util.checkDeps(item.Department))
                        {



                            var row = dt.NewRow();
                            row["id"] = item.EmployeeID;
                            row["name"] = item.FirstName + " " + item.LastName;
                            dt.Rows.Add(row);

                        }
                    }
                }
            }
            
            return dt;
        }
        public void EventDelete(string id)
        {
            var employeeList = getList();
            var indexEmp ="";
            var indexEvent = "";
            DataAccess.DAL_BYD dataAccess = new DataAccess.DAL_BYD();
            for (int i = 0; i < employeeList.Count; i++)
            {
                for (int p = 0; p < employeeList[i].Events.Count; p++)
                {
                    if (employeeList[i].Events[p].EventID == id)
                    {
                        indexEmp = i.ToString();
                        indexEvent = p.ToString();
                        dataAccess.DeleteSAPTimes(employeeList[i].Events[p].SAP_UUID);
                    }
                }
            }

            if (indexEmp != "" && indexEvent!="")
            {
                employeeList[Convert.ToInt16(indexEmp)].Events.RemoveAt(Convert.ToInt16(indexEvent));
            }   
            saveList(employeeList as List<Employee>);

        }

        public void EventMove(string id, DateTime start, DateTime end, string resource)
        {
            var employeeList = getList();
            DataAccess.DAL_BYD dataAccess = new DataAccess.DAL_BYD();
            string selectedEventID = "";
            TimeSpan diff = end - start;
            int days = diff.Days; 

            foreach (var emp in employeeList)
            {
                foreach (var _event in emp.Events)
                {

                    if (_event.EventID == id)
                    {
                        if (_event.Description != "Abwesenheit")
                        {
                            if (_event.SAP_UUID != null)
                            {
                                _event.Start_Date = start;
                                //if (days>1)
                                //{
                                //    _event.End_Date = end.AddDays(-1);
                                //}
                                //else
                                //{
                                    _event.End_Date = end;
                                //}
                                selectedEventID = _event.SAP_UUID;
                                dataAccess.UpdateSAPevents(_event.SAP_UUID, start, end, resource);

                            }
                        }
                      

                    }
                }
               
            }
            if (selectedEventID!="")
            {
                updateListAfterDrag(selectedEventID, resource);
            }

            saveList(employeeList as List<Employee>);
        }

        public Event Get(string id)
        {
            var employeeList = getList();
            DataTable dt2 = BuildStandartTable();

            foreach (var employee in employeeList)
            {

                foreach (var _event in employee.Events)
                {
                    if (_event.EventID == id)
                    {
                        var row = dt2.NewRow();
                        row["id"] = _event.EventID;
                        row["name"] = _event.Description;
                        row["eventstart"] = _event.Start_Date;
                        row["eventend"] = _event.End_Date;
                        row["resource"] = employee.FirstName + " " + employee.LastName;
                        row["tag"] = "test";
                        dt2.Rows.Add(row);
                    }

                }

            }

            DataTable dt = new DataTable();
            // da.Fill(dt);
            
            if (dt2.Rows.Count > 0)
            {
                Event retEvent = new Event();
                DataRow dr2 = dt2.Rows[0];
                retEvent.Id = id;
                retEvent.Text = dr2["name"].ToString();
                
                retEvent.Start = Convert.ToDateTime(dr2["eventstart"].ToString());
                retEvent.End = Convert.ToDateTime(dr2["eventend"].ToString());
                retEvent.End = retEvent.End.Date;
                retEvent.Resource = dr2["resource"].ToString();
                return retEvent;
            }
            return null;
        }

        public IEnumerable<SelectListItem> ResourceSelectList()
        {


            return
                GetResources().AsEnumerable().Select(u => new SelectListItem
                                                              {
                                                                  Value = u.Field<string>("id"),
                                                                  Text = u.Field<string>("name")
                                                              });
        }

        public Event EventCreate(DateTime start, DateTime end, string text, string resource, string SAP_UUID)
        {
            var emplist = getList();
            string id = SAP_UUID;

            foreach (var _employee in emplist)
            {
                if (_employee.EmployeeID == (resource))
                {
                    _employee.Events.Add(new AbsenceDay(id, text, start, end, SAP_UUID));
                }
            }

            saveList(emplist);


            Event retEvent = new Event();

            retEvent.End = end;
            retEvent.Id = id;
            retEvent.Resource = getDisplaynameByID(resource);
            retEvent.Start = start;
            retEvent.Text = text;
            return retEvent;


        }

        public Event EventCreate(DateTime start, DateTime end, string text, string resource, string SAP_UUID, RecurrenceRule rule)
        {
            var emplist = getList();
            string id = SAP_UUID;

            foreach (var _employee in emplist)
            {
                if (_employee.EmployeeID == (resource))
                {
                    _employee.Events.Add(new AbsenceDay(id, text, start, end, SAP_UUID, rule));
                }
            }

            saveList(emplist);


            Event retEvent = new Event();

            retEvent.End = end;
            retEvent.Id = id;
            retEvent.Resource = getDisplaynameByID(resource);
            retEvent.Start = start;
            retEvent.Text = text;
            retEvent.Recurrence = rule;
            
            return retEvent;


        }


        public Event makeEvent(DateTime start, DateTime end, string res, RecurrenceRule rule)
        {
            Event retEvent = new Event();
            retEvent.Start = start;
            retEvent.End = end;
            retEvent.Resource = getDisplaynameByID(res);
            retEvent.Recurrence = rule;
            return retEvent;

        }


        public class Event
        {
            public string Id { get; set; }
            public string Text { get; set; }
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
            public string Resource { get; set; }
            public SelectList TimeType { get; set; }
            public string Type { get; set; }
            public RecurrenceRule Recurrence { get; set; }
        }


        public void updateListAfterDrag(string uuid, string newEmp)
        {
            var employeeList = getList();
            var indexEmp = "";
            var indexEvent = "";
            AbsenceDay selecetEvent = new AbsenceDay();
            for (int i = 0; i < employeeList.Count; i++)
            {
                for (int p = 0; p < employeeList[i].Events.Count; p++)
                {
                    if (employeeList[i].Events[p].EventID == uuid)
                    {
                        indexEmp = i.ToString();
                        indexEvent = p.ToString();
                        selecetEvent = employeeList[i].Events[p]; 
                    }
                }
            }
            if (indexEmp != "" && indexEvent != "")
            {
                employeeList[Convert.ToInt16(indexEmp)].Events.RemoveAt(Convert.ToInt16(indexEvent));
            }
            foreach (var emp in employeeList)
            {
                if (emp.EmployeeID==newEmp)
                {
                    emp.Events.Add(selecetEvent);
                }
            }
            saveList(employeeList as List<Employee>);

        }

        public string getIDByDisplayname (string displayname)
        {
            var emplist = getList();
            string id = "";
            foreach (var employee in emplist)
            {
                if (employee.FirstName+" "+employee.LastName == displayname)
                {
                    id = employee.EmployeeID;
                }
                else
                {
                    id = "";
                }
            }
            return id;
        }
        public string getDisplaynameByID(string id)
        {
            string displayname = "";
            var emplist = getList();

            foreach (var employee in emplist)
            {
                if (employee.EmployeeID == id)
                {
                    displayname = employee.FirstName + " " + employee.LastName;
                    break;  
                }
                else
                {
                    displayname="";
                }
            }
            return displayname;
        }

        public void copyPaste(string uuid, string oldEmpID, string newEmpID, DateTime Start, int days)
        {
            AbsenceDay buffer = new AbsenceDay();
            DataAccess.DAL_BYD dataAccess = new DataAccess.DAL_BYD();
            DateTime end = new DateTime();

            var emplist = getList();

            foreach (var emp in emplist)
            {
                if (emp.EmployeeID==oldEmpID)
                {
                    foreach (var proj in emp.Events)
                    {
                        if (uuid== proj.SAP_UUID)
                        {
                            buffer = proj;
                        }

                    }
                }
            }
            string projID = "";
            string[] bufferProjID = buffer.Description.Split(' ');
            projID = bufferProjID[0];
            if (days >0)
            {
               end= Start.AddDays(days - 1);
            }
            else
            {
                 end = Start;
            }
            
           string SAP_UUID = dataAccess.SaveTimes(Start.ToString(), end.ToString(), projID, newEmpID);
            var test = EventCreate(Start, end, buffer.Description, newEmpID, SAP_UUID);
        }

        public IList<Employee> getList()
        {

            return HttpContext.Current.Session["emplist"] as IList<Employee>;
        }
        public void saveList(IList<Employee> emplist)
        {
            HttpContext.Current.Session["emplist"] = emplist;
        }

      
    }

}