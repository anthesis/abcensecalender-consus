﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Enums;
using DayPilot.Web.Mvc.Events.Scheduler;
using System.Drawing;
using DayPilot.Web.Mvc.Data;
using TutorialCS.Models;

namespace TutorialCS.Controllers
{
    public class SchedulerController : Controller
    {
        //
        // GET: /Scheduler/

        public ActionResult Backend()
        {
            return new Dps().CallBack(this);
        }

        class Dps : DayPilotScheduler
        {
            protected override void OnInit(InitArgs e)
            {
             
                LoadResources();
                //UpdateWithMessage("Welcome!", CallBackUpdateType.Full);
                Update(CallBackUpdateType.Full);
            }

            private void LoadResources()
            {
                foreach (DataRow r in new EventManager().GetResources().Rows)
                {
                    Resources.Add((string)r["name"], Convert.ToString(r["id"]));
                }
            }

            protected override void OnEventResize(EventResizeArgs e)
            {
                new EventManager().EventMove(e.Id, e.NewStart, e.NewEnd, e.Resource);
                UpdateWithMessage("The event was resized.");
            }

            protected override void OnEventMove(EventMoveArgs e)
            {
                new EventManager().EventMove(e.Id, e.NewStart, e.NewEnd, e.NewResource);
                UpdateWithMessage("The event was moved.");
            }

            protected override void OnTimeRangeSelected(TimeRangeSelectedArgs e)
            {
                if (CopyedEvent.eventRessource!= null && CopyedEvent.eventUUID != null)
                {
                    new EventManager().copyPaste(CopyedEvent.eventUUID, CopyedEvent.eventRessource, e.Resource, e.Start,CopyedEvent.TimeLenght);
         
                }
           
                UpdateWithMessage("A event was copied.");
            }
            protected override void OnEventMenuClick(EventMenuClickArgs e)
            {
                switch (e.Command)
                {
                    case "Delete":
                        new EventManager().EventDelete(e.Id);
                        Update();
                        break;
                    case "test":
                        var test = e;
                        
                        break;
                }
            }

            protected override void OnEventDoubleClick(EventDoubleClickArgs e)
            {
                CopyedEvent.eventUUID = e.Id;
                CopyedEvent.eventRessource = e.Resource;
                TimeSpan diff = e.End - e.Start;
                CopyedEvent.TimeLenght = diff.Days;
            }
            protected override void OnEventSelect(EventSelectArgs e)
            {
                var test = e;
            }
            protected override void OnEventDelete(EventDeleteArgs e)
            {
                string ID = "";
                if (e.Id != null ||e.RecurrentMasterId!= null) { 
                    if (e.Id ==null)
                    {
                        ID = e.RecurrentMasterId;
                        new EventManager().EventDelete(ID);
                    }

                   else
                    {
                        new EventManager().EventDelete(e.Id);
                    }
                }
                UpdateWithMessage("Event deleted.");
            }
            protected override void OnEventRightClick(EventRightClickArgs e)
            {
                CopyedEvent.eventUUID = e.Id;
                CopyedEvent.eventRessource = e.Resource;
            }

            protected override void OnCommand(CommandArgs e)
            {
                switch (e.Command)
                {
                    case "refresh":
                        UpdateWithMessage("Refreshed");
                        break;
                }
            }
            protected override void OnBeforeCellRender(BeforeCellRenderArgs e)
            {
                if (e.Start.DayOfWeek.ToString() =="Saturday" || e.Start.DayOfWeek.ToString() == "Sunday")
                {
                    e.BackgroundColor = "#d3d3d3";
                }
            }

            protected override void OnBeforeEventRender(BeforeEventRenderArgs e)
            {
                var test = e;
                var test2 = e.DataItem;
                var test3 = e.DataItem["name"];
                string buffer = test3.ToString();
               
                if (buffer.Contains("Abwesenheit"))
                {
                    e.BackgroundColor = "#ee5c42";
                    e.FontColor = "#FFFFFF";
                }
                else
                {
                    e.ContextMenuClientName = "menu";
                    int color = buffer.GetHashCode() & 0xFFFFFF;
                    string hex = color.ToString("X");
                    if (hex.Length<6)
                    {
                        for (int i = 0; i < 6-hex.Length; i++)
                        {
                            hex = "0" + hex;
                        }
                    }

                    hex = "#" + hex;

                    Color fromHex = Color.FromArgb(color);


                    // Counting the perceptive luminance - human eye favors green color... 
                    double a = 1 - (0.299 * fromHex.R + 0.587 * fromHex.G + 0.114 * fromHex.B) / 255;

                    if (a < 0.5) { 
                        e.FontColor = "#000000"; // bright colors - black font
                    }
                    else { 
                        e.FontColor = "#FFFFFF";
                    }
                   
                    e.BackgroundColor = hex;
                    if (e.RecurrentMasterId!= null)
                    {
                        e.BorderColor = "#000000";
                        
                    }
                    else
                    {
                        e.BorderColor = e.BackgroundColor;
                    }


                    e.Areas.Add(new Area().Width(17).Bottom(9).Right(2).Top(3).CssClass("event_action_menu").ContextMenu("menu"));
                }
                //if ((string)e.DataItem["type"] == "special")  // "type" field must be available in the DataSource
                //{
                //    e.CssClass = "special";
                //    e.BackgroundColor = "lightyellow";
                //    e.Html = "<i>WARNING: This is an unusual event.</i><br>" + e.Html;
                //}


                

                //e.Areas.Add(new Area().Width(17).Bottom(9).Right(2).Top(3).CssClass("event_action_delete").JavaScript("dps.eventDeleteCallBack(e);"));
            }

            protected override void OnFinish()
            {
                if (UpdateType == CallBackUpdateType.None)
                {
                    return;
                }

                Events = new EventManager().FilteredData(StartDate, StartDate.AddDays(Days)).AsEnumerable();

                DataIdField = "id";
                DataTextField = "name";
                DataStartField = "eventstart";
                DataEndField = "eventend";
                DataResourceField = "resource";
                DataTagFields= "tag";
                DataRecurrenceField = "recurrence";
            }

        }

    }
}
