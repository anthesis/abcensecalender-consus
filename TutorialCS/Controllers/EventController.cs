﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DayPilot.Web.Mvc.Json;
using TutorialCS;
using TutorialCS.Models;
using System.Collections;
using TutorialCS.DataAccess;
using TutorialCS.Util;
using DayPilot.Web.Mvc.Recurrence;

public class EventController : Controller
{

    public ActionResult Edit(string id)
    {
        EditViewModel editView = new EditViewModel();
            var e = new EventManager().Get(id) ?? new EventManager.Event();
        Util util = new Util();
        TimeSpan test = e.End-e.Start;
        int diff = test.Days;

        editView.ID = e.Id;
        editView.Eventname = e.Text;
        editView.StartDate = e.Start;

        List<Employee> emplist = Session["emplist"] as List<Employee>;
        AbsenceDay Event = new AbsenceDay();



        foreach (var emp in emplist)
        { 
          var hol= emp.Events.Where(x => x.EventID == id);
            foreach (var realHol in hol)
            {
                Event = realHol;
            }
        }

        editView.ID = Event.EventID;
        editView.Eventname = Event.Description;
        editView.StartDate = Event.Start_Date;
        //if (diff ==1)
        //{
        //    editView.EndDate = e.End.AddDays(-1);
        //}
        //else
        //{
        editView.EndDate = Event.End_Date;
        //}

        editView.EmployeeName = e.Resource;
        var drop = util.GetRecurringList();
        editView.recurrDropList = util.GetRecurringList();
        editView.SelectedValue = EditViewModel.Selection.Option3;
        if (Event.recRule != null)
        {
            var rangeSpec = Event.recRule.RangeSpec;
            if (rangeSpec == RecurrenceRule.RangeSpecType.Times)
            {
                editView.recAmount = Event.recRule.TimesValue;
            }
            int index = 0;
            switch (Event.recRule.Repeat)
            {
                case RecurrenceRule.RepeatRule.Weekly:
                    if (Event.recRule.Every == 1)
                    {
                        index = 1;
                    }
                    else
                    {
                        index = 2;
                    }

                    break;
                case RecurrenceRule.RepeatRule.Daily:
                    index = 0;
                    break;
                case RecurrenceRule.RepeatRule.Annually:
                    index = 3;
                    break;
                default:
                    break;
            }
            editView.recurrDropList[index].Selected = true;
            //var test2 = e.Recurrence.Id;
        }


        return View(editView);
    }



    [AcceptVerbs(HttpVerbs.Post)]
    public ActionResult Edit(EditViewModel model)
    {
        DateTime start = Convert.ToDateTime(model.StartDate);
        DateTime end = Convert.ToDateTime(model.EndDate);
        new EventManager().EventEdit(model.ID, model.Eventname, start, end, model.EmployeeName);
        return JavaScript(SimpleJsonSerializer.Serialize("OK"));
    }


    public ActionResult Create()
    {
        Util util = new Util();
        CreateModel model = new CreateModel();
        List<Employee> emplist = Session["emplist"] as List<Employee>;

        List<Project> forChosen = new List<Project>();
        List<Employee> forChosenEmp = new List<Employee>();


  

        string id = Request.QueryString["resource"];
        model.SelectedEmployee = util.getEmployeeByID(id);
        model.SelectedEmployee.dispname = model.SelectedEmployee.EmployeeID + " " + model.SelectedEmployee.FirstName + " " + model.SelectedEmployee.LastName;


        model.StartDate = Convert.ToDateTime(Request.QueryString["start"]);
        model.EndDate = Convert.ToDateTime(Request.QueryString["end"]);

        //TimeSpan test = model.EndDate.Value - model.StartDate.Value;
        //int diff = test.Days;

        //if (diff>0)
        //{
        //    model.EndDate = model.EndDate.Value.AddDays(-1);
        //}
        //else
        //{
        //    model.EndDate = model.EndDate;
        //}


            foreach (var project in model.SelectedEmployee.Projects)
            {
              
                    forChosen.Add(project);
            }

        model.projectList = forChosen;
        model.projectDropList = prepareProjectTropdown(forChosen);

        model.employeeList = emplist;
        int index = emplist.IndexOf(model.SelectedEmployee);
        model.EmpDropDown = prepareEmpDropdown(model.employeeList);
        model.EmpDropDown[index].Selected = true;

        model.recurrDropList = util.GetRecurringList();
        Session["RecurrDrop"] = model.EmpDropDown;

        Session["EmpDropdown"] = model.EmpDropDown;
       

        Session["CreateProjectDropdown"] = model.projectDropList;



        return View(model);

    }
    public ActionResult CreateFromDropdown()
    {
        CreateFromProjectDropDownModel model = new CreateFromProjectDropDownModel();
        List<Employee> emplist = Session["emplist"] as List<Employee>;
        List<Employee> forChosen = new List<Employee>();
        Util util = new Util();

        string id = Request.QueryString["id"];
        
        model.SelectedProject = util.getProjectFromID(id);
        model.SelectedProject.dispName = model.SelectedProject.ProjectId + " " + model.SelectedProject.ProjectName;
        model.StartDate = null;
        model.EndDate = null;

        foreach (var emp in emplist)
        {
            foreach (var project in emp.Projects)
            {
                if (project.ProjectId == model.SelectedProject.ProjectId)
                {
                    forChosen.Add(emp);
                }
            }
        }
        
        model.employeeList = forChosen;
        model.EmpDropDown = prepareEmpDropdown(model.employeeList);
        Session["EmpDropdown"] = model.EmpDropDown;
        model.recurrDropList = util.GetRecurringList();
        


       // return View(new EventManager().makeEvent2(Request.QueryString["id"], Request.QueryString["start"], Request.QueryString["end"], Request.QueryString["resource"]));
        return View(model);
    }

    [HttpPost]
    public ActionResult CreateFromDropdown(CreateFromProjectDropDownModel model)
    {

        foreach (var item in model.SelectedID)
        {
            DAL_BYD dataAccess = new DAL_BYD();
            Util util = new Util();
            string empID = util.getEmpFromDropdown(item.ToString());
            string SAP_UUID = "";
            string[] buffer = empID.Split(' ');
            empID = buffer[0];


            DateTime start = Convert.ToDateTime(model.StartDate);
            DateTime end = Convert.ToDateTime(model.EndDate);


            string sStart = util.ConvertToABAPTime(model.StartDate.Value);
            string sEnd = util.ConvertToABAPTime(model.EndDate.Value);

            string[] buffer2 = model.SelectedProject.dispName.Split(' ');
            string projid = buffer2[0];
            string projname = "";
            for (int i = 1; i < buffer2.Length; i++)
            {
                projname = projname + buffer2[i] + " ";
            }

            DateTime sRecEnd = new DateTime();
            int recTimes = 0;
            bool unlimited = false;

            if (model.SelectedValue == Selection.Option1)
            {
                unlimited = true;
            }


            if (model.SelectedRecurrID != null)
            {
                string recType = util.getRecType(model.SelectedRecurrID[0]);

                switch (model.SelectedValue)
                {
                    case Selection.Option1:
                        break;
                    case Selection.Option2:
                        recTimes = getRecTimes(recType, (DateTime)model.StartDate, (DateTime)model.RecEndDate);
                        sRecEnd = (DateTime)model.RecEndDate;
                        break;
                    case Selection.Option3:
                        recTimes = model.recAmount;
                        break;
                    default:
                        break;
                }
                if (unlimited)
                {
                    SAP_UUID = dataAccess.SaveTimes(sStart, sEnd, projid, empID, recType, unlimited);
                }

                else
                {
                    SAP_UUID = dataAccess.SaveTimes(sStart, sEnd, projid, empID, recType, recTimes);
                }

                RecurrenceRule rule = null;
                switch (model.SelectedValue)
                {
                    case Selection.Option1:
                        rule = util.createRecRuleUnlimited(recType, start, SAP_UUID);
                        break;
                    case Selection.Option2:
                        rule = util.createRecRule(recType, start, (DateTime)model.RecEndDate, SAP_UUID);
                        break;
                    case Selection.Option3:
                        rule = util.createRecRuleTimes(recType, start, model.recAmount, SAP_UUID);
                        break;
                    default:
                        break;
                }
                new EventManager().EventCreate(start, end, projname, empID, SAP_UUID, rule);
            }
            else
            {
                SAP_UUID = dataAccess.SaveTimes(sStart, sEnd, projid, empID);
                new EventManager().EventCreate(start, end, projname, empID, SAP_UUID);

            }
        }
        return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }
   



    [HttpPost]
    public ActionResult Create(CreateModel model)
    {
        foreach (var item in model.SelectedID)
        {
            foreach (var item2 in model.SelectedEmpID)
            {
                DAL_BYD dataAccess = new DAL_BYD();
                Util util = new Util();

                string SAP_UUID = "";
                DateTime start = Convert.ToDateTime(model.StartDate);
                DateTime end = Convert.ToDateTime(model.EndDate);             

                string sStart = util.ConvertToABAPTime(model.StartDate.Value);
                string sEnd = util.ConvertToABAPTime(model.EndDate.Value);
                       
                bool unlimited = false;
                
                if (model.SelectedValue== Selection.Option1)
                {
                    unlimited = true;    
                }

                string empID = util.getEmpFromDropdown(item2.ToString());
                string[] buffer = empID.Split(' ');
                empID = buffer[0];
                string projectName = util.getProjectNameFromCreate(item.ToString());
                string projectID = util.getProjectIDFromName(projectName);
                //SAP_UUID = dataAccess.SaveTimes(sStart, sEnd, projectID, empID);

                DateTime sRecEnd = new DateTime();
                int recTimes = 0;

                if (model.SelectedRecurrID!=null)
                {
                    string recType = util.getRecType(model.SelectedRecurrID[0]);

                    switch (model.SelectedValue)
                    {
                        case Selection.Option1:
                            break;
                        case Selection.Option2:
                            recTimes = getRecTimes(recType, (DateTime)model.StartDate, (DateTime)model.RecEndDate);
                            sRecEnd = (DateTime)model.RecEndDate;
                            break;
                        case Selection.Option3:
                            recTimes = model.recAmount;
                            break;
                        default:
                            break;
                    }
                    if (unlimited)
                    {
                        SAP_UUID = dataAccess.SaveTimes(sStart, sEnd, projectID, empID, recType, unlimited);
                    }

                    else
                    {
                        SAP_UUID = dataAccess.SaveTimes(sStart, sEnd, projectID, empID, recType,recTimes);
                    }

                    RecurrenceRule rule =null ;
                    switch (model.SelectedValue)
                    {
                        case Selection.Option1:
                             rule = util.createRecRuleUnlimited(recType, start, SAP_UUID);
                            break;
                        case Selection.Option2:
                             rule = util.createRecRule(recType, start, (DateTime)model.RecEndDate, SAP_UUID);
                            break;
                        case Selection.Option3:
                            rule = util.createRecRuleTimes(recType, start, model.recAmount, SAP_UUID);
                            break;
                        default:
                            break;
                    }
                    new EventManager().EventCreate(start, end, projectName, empID, SAP_UUID, rule);
                }
                else
                {
                    SAP_UUID = dataAccess.SaveTimes(sStart, sEnd, projectID, empID);
                    new EventManager().EventCreate(start, end, projectName, empID, SAP_UUID);

                }              
            }
           
        }
        return JavaScript(SimpleJsonSerializer.Serialize("OK"));
    }


 


    private IList<SelectListItem> prepareProjectTropdown(List<Project> projectList)
    {
        List<SelectListItem> retList = new List<SelectListItem>();
        int i = 1;
        foreach (var project in projectList)
        {
            if (!String.IsNullOrEmpty(project.ProjectName) || !String.IsNullOrEmpty(project.ProjectId))
            {
                string dispName = project.ProjectId + " " + project.ProjectName;
                retList.Add(new SelectListItem { Text = dispName, Value = i.ToString() });
            }
            i++;
        }
        return retList;
    }
    private IList<SelectListItem> prepareEmpDropdown(List<Employee> empList)
    {
        List<SelectListItem> retList = new List<SelectListItem>();

        int i = 1;
        foreach (var employee in empList)
        {
            if (!String.IsNullOrEmpty(employee.FirstName) || !String.IsNullOrEmpty(employee.EmployeeID))
            {
                string dispName = employee.EmployeeID + " " + employee.FirstName + " " + employee.LastName;
                retList.Add(new SelectListItem { Text = dispName, Value = i.ToString() });
            }
            i++;
        }
        return retList;   
    }




    public int getRecTimes(string recType, DateTime start, DateTime recEnd)
    {
        Util util = new Util();
        int times = 0;
        switch (recType)
        {
            case "Täglich":
                times = util.getTimesDaily(start, recEnd);

                break;
            case "Wöchentlich":
                times = util.getTimesWeekly(start, recEnd);

                break;
            case "Zwei-Wöchentlich":
                times = util.getTimesBiWeekly(start, recEnd);
                break;
            case "Monatlich":
                times = util.getTimesMonthly(start, recEnd);
                break;
            case "Jährlich":
                times = util.getTimesYearly(start, recEnd);
                break;
            default:
                break;
    }

        return times;
    }
}
