﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TutorialCS.Models;

namespace TutorialCS.Views.Account
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            // shows login page.
            return View();
        }

        
        [HttpPost]
        public ActionResult Login(UserLogin model)
        {
            DataAccess.DAL_BYD ByDAccess = new DataAccess.DAL_BYD();
            if (string.IsNullOrEmpty(model.User) || string.IsNullOrEmpty(model.User))
            {
               return RedirectToAction("Login");
            }

            else
            {
                // if credentials are correct.
                Session["Username"] = model.User;
                Session["Password"] = model.Password;


                switch (ByDAccess.getAllEmployees())
                {
                    case 1:
                        return RedirectToAction("Error");
                    case 2:
                        Session["StartDate"] = new DateTime(2011, 1, 1);
                        return RedirectToAction("Index","Home");
                    default:
                        return RedirectToAction("Login");
                }
            }
        }
        private static void UpdateSetting(string key, string value)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");
        }
        public ActionResult Error()
        {
            // shows login page.
            return View();
        }
    }
}