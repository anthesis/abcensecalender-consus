﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using TutorialCS;
using TutorialCS.Models;
using TutorialCS.DataAccess;

namespace TutorialCS.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            Session["EndDate"] = 365;

            if (Session["emplist"] == null)
            {
                Session["ERROR"] = "Bitte erneut anmelden";
                return RedirectToAction("Error", "Account");
            }

            

            ViewModel model = new ViewModel();
            var list = new List<Department>();
            list = Session["DepList"] as List<Department>;
            model.Departmentlist = list;
            
             var projectList = new List<Project>();
            projectList = Session["ProjList"] as List<Project>;
            model.ProjectList = projectList;

            model.DepartDropDown = new List<SelectListItem>();
            model = FillDepDropDown(model);
            Session["DepDropdownData"] = model.DepartDropDown;

            model.ProjectDropDown = new List<SelectListItem>();
            model = FillProjectDropDown(model);
            Session["ProjectDropdownData"] = model.ProjectDropDown;

            model.FilterProjectDropDown = new List<SelectListItem>();
            model = FilterProjectDropDown(model);
            Session["FilterProjectDropdown"] = model.FilterProjectDropDown;
            Session["StartDate"] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            Session["AutoWidth"] = 1;




            return View(model);
        }

  
        [HttpPost]
        public ActionResult Index(ViewModel model)
        {
            int days = 365;

            model.DepartDropDown = Session["DepDropdownData"] as IList<SelectListItem>;
            model.FilterProjectDropDown = Session["FilterProjectDropdown"] as IList<SelectListItem>;
            if (model.SelectedIds!=null)
            {
                PrepareDepList(model.SelectedIds);
            }
            else
            {
                model = PrepareDepListNull(model);
            }

            if (model.FilterProjectSelectedID != null)
            {
                PrepareProjectList(model.FilterProjectSelectedID);
            }
            else
            {
                model = PrepareProjectListNull(model);
            }


            //Session["DepList"] = model.Departmentlist;

            if (model.StartDate!=null)
            {
                Session["StartDate"] = model.StartDate;
                if (model.EndDate != null)
                {
                    TimeSpan diff = model.EndDate.Value - model.StartDate.Value;
                    if ((int)diff.TotalDays > 0)
                    {
                        days = (int)diff.TotalDays;
                        Session["EndDate"] = days+1;

                    }
                }
                else
                {
                    Session["EndDate"] = 365;
                }
               
            }
            else
            {
                Session["StartDate"] = DateTime.Today;
            }
            if (days<30)
            {
                Session["AutoWidth"] = 0;
            }
            else
            {
                Session["AutoWidth"] = 1;
            }
            model.ProjectDropDown = Session["ProjectDropdownData"] as List<SelectListItem>;
            return View(model);
        }

        public ActionResult LogOut() {

            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("Login","Account");
        }


        private ViewModel FillDepDropDown(ViewModel model)
        {
            int i = 1;
            foreach (var dep in model.Departmentlist)
            {
                if (!String.IsNullOrEmpty(dep.Name)||!String.IsNullOrEmpty(dep.id))
                {
                    model.DepartDropDown.Add(new SelectListItem { Text = dep.Name, Value = i.ToString() });
                }
                i++;
             }

            return model;
        }

        private ViewModel FillProjectDropDown(ViewModel model)
        {
            int i = 2;
            model.ProjectDropDown.Add(new SelectListItem { Text = "", Value ="1" });
            foreach (var project in model.ProjectList)
            {
                if (!String.IsNullOrEmpty(project.ProjectName) || !String.IsNullOrEmpty(project.ProjectId))
                {
                    model.ProjectDropDown.Add(new SelectListItem { Text = project.ProjectName, Value = i.ToString() });
                }
                i++;
            }

            return model;
        }
        private ViewModel FilterProjectDropDown(ViewModel model)
        {
            int i = 1;
      
            foreach (var project in model.ProjectList)
            {
                if (!String.IsNullOrEmpty(project.ProjectName) || !String.IsNullOrEmpty(project.ProjectId))
                {
                    model.FilterProjectDropDown.Add(new SelectListItem { Text = project.ProjectId +" "+project.ProjectName, Value = i.ToString() });
                }
                i++;
            }

            return model;
        }
        private void PrepareDepList(int[] SelectedIndexes)
        {
            List<SelectListItem> DepDropdownData = Session["DepDropdownData"] as List<SelectListItem>;
            var list = new List<Department>();
            
            list = Session["DepList"] as List<Department>;

            List<string> selectedNames = new List<string>();

            List<Department> dep = new List<Department>();

            foreach (var indexes in SelectedIndexes)
            {
                foreach (var item in DepDropdownData)
                {
                    if (indexes.ToString() == item.Value)
                    {
                        selectedNames.Add(item.Text);
                       
                    }
                }
            }
            getSelectedDepartments(selectedNames);
        }

          

         private void getSelectedDepartments(List<string> selectedNames)
        {
            var deplist = new List<Department>();
            deplist = Session["DepList"] as List<Department>;

            List<string> depnames = new List<string>();

            List<int> indexesOfDepList = new List<int>();

            foreach (var department in deplist)
            {
                department.CheckedValue = false;
            }

            foreach (var department in deplist)
            {
                depnames.Add(department.Name);
            }

            foreach (var names in selectedNames)
            {
                indexesOfDepList.Add(depnames.IndexOf(names));
            }
            foreach (var index in indexesOfDepList)
            {
                deplist[index].CheckedValue = true;
            }
  
           
            Session["DepList"] = deplist;
        }

        private ViewModel PrepareDepListNull(ViewModel model)
        {
            var list = new List<Department>();

            list = Session["DepList"] as List<Department>;

            foreach (var item in list)
            {
                item.CheckedValue = false;
            }
            Session["DepList"] = list;

            foreach (var item in model.DepartDropDown)
            {
                item.Selected = false;
            }
            return model;
        }



        private ViewModel PrepareProjectListNull(ViewModel model)
        {
            var list = new List<Project>();

            list = Session["ProjList"] as List<Project>;

            foreach (var item in list)
            {
                item.CheckedValue = false;
            }
            Session["ProjList"] = list;

            foreach (var item in model.FilterProjectDropDown)
            {
                item.Selected = false;
            }
            return model;
        }

        private void PrepareProjectList(int[] SelectedIndexes)
        {
            List<SelectListItem> FilterProjectDropdownData = Session["FilterProjectDropdown"] as List<SelectListItem>;
            var list = new List<Project>();

            list = Session["ProjList"] as List<Project>;

            List<string> selectedNames = new List<string>();

            List<Project> project = new List<Project>();

            foreach (var indexes in SelectedIndexes)
            {
                foreach (var item in FilterProjectDropdownData)
                {
                    if (indexes.ToString() == item.Value)
                    {
                        selectedNames.Add(item.Text);

                    }
                }
            }
            getSelectedProjects(selectedNames);
        }



        private void getSelectedProjects(List<string> selectedNames)
        {
            var projectplist = new List<Project>();
            projectplist = Session["ProjList"] as List<Project>;

            List<string> projectnames = new List<string>();

            List<int> indexesOfProjectList = new List<int>();

            foreach (var department in projectplist)
            {
                department.CheckedValue = false;
            }

            foreach (var department in projectplist)
            {
                projectnames.Add(department.ProjectId);
            }

            foreach (var names in selectedNames)
            {
                string[] buffer = names.Split(' ');
                string search=buffer[0]; 


                indexesOfProjectList.Add(projectnames.IndexOf(search));
            }
            foreach (var index in indexesOfProjectList)
            {
                projectplist[index].CheckedValue = true;
            }


            Session["ProjList"] = projectplist;
        }

    }
    }

