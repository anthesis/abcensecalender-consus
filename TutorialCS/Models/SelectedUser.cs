﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TutorialCS.Models
{
    public static class SelectedUser
    {   
        public static string SelectedUserName { get; set; }
        public static bool seeDep { get; set; }
        public static bool seeAll { get; set; }
        public static string SelectedDepID { get; set; }
        public static string SelectedDepName { get; set; }
    }
}