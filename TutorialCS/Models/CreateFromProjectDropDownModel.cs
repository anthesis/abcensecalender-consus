﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TutorialCS.Models
{
    public class CreateFromProjectDropDownModel
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        [Required]
        public List<Employee> employeeList { get; set; }

        public Project SelectedProject { get; set; }

        public int[] SelectedID { get; set; }
        [Required]
        public IList<SelectListItem> EmpDropDown { get; set; }

        public int[] SelectedRecurrID { get; set; }

        public IList<SelectListItem> recurrDropList { get; set; }

        public Selection SelectedValue { get; set; }


        public int recAmount { get; set; }
        public bool unlimited { get; set; }
        [DataType(DataType.Date)]
        public DateTime? RecEndDate { get; set; }
    }
}