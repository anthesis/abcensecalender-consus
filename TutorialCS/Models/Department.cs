﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace TutorialCS.Models
{
    
    public class Department
    {
        
        public bool CheckedValue { get; set; }

        public string Name { get; set; }
        public string id { get; set; }


        public Department (bool _checkedValue, string _DepName,string _id)
        {
            CheckedValue = _checkedValue;
            Name = _DepName;
            id = _id;
        }
        public Department()
        {

        }
    }
}