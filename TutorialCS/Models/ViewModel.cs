﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TutorialCS.Models
{
    public class ViewModel
    {
        public List<Department> Departmentlist { get; set; }

        public List<Project> ProjectList { get; set; }



        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        public IList<SelectListItem> DepartDropDown { get; set; }
        public IList<SelectListItem> FilterProjectDropDown { get; set; }
        public IList<SelectListItem> ProjectDropDown { get; set; }


        public int[] SelectedIds { get; set; }
        public int[] ProjectSelectedID { get; set; }
        public int[] FilterProjectSelectedID { get; set; }

        public ViewModel ()
        {
            
            }
    }
}