﻿using DayPilot.Web.Mvc.Recurrence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TutorialCS.Models;

/// <summary>
/// Summary description for Event
/// </summary>
public class AbsenceDay
{
    public string EventID { get; set; }
    public string Description { get; set; }
    public DateTime Start_Date { get; set; }
    public DateTime End_Date { get; set; }
    public string SAP_UUID { get; set; }
    public string recRuleString { get; set; }
    public RecurrenceRule recRule { get; set; }

    public AbsenceDay(string id, string text, DateTime start, DateTime end)
    {
        EventID = id;
        Description = text;
        Start_Date = start;
        End_Date = end;
    }
    public AbsenceDay(string id, string text, DateTime start, DateTime end, string _SAP_UUID)
    {
        EventID = id;
        Description = text;
        Start_Date = start;
        End_Date = end;
        SAP_UUID = _SAP_UUID;
    }
    public AbsenceDay(string id, string text, DateTime start, DateTime end, string _SAP_UUID, RecurrenceRule rule)
    {
        EventID = id;
        Description = text;
        Start_Date = start;
        End_Date = end;
        SAP_UUID = _SAP_UUID;
        recRule = rule;
    }
   
    public AbsenceDay()
    {

    }
}