﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TutorialCS.Models
{
    public class RecurringModelForViews
    {
       public List<string> recurringTypes { get; set; }
       public string selectedAnswer { get; set; }
    }
}