﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TutorialCS.Models
{
    public class Project
    {
        public string ProjectName { get; set; }
        public string ProjectId { get; set; }
        public string dispName { get; set; }

        public bool CheckedValue { get; set; }

        public Project(string _ProjectName, string _ProjectId)
        {
            ProjectName = _ProjectName;
            ProjectId = _ProjectId;
        }

        public Project()
        {
        }
    }
}