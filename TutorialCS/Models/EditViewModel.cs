﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TutorialCS.Models
{
    public class EditViewModel
    {
        public string Eventname { get; set; }

        public string ID { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        public string EmployeeName { get; set; }
        public Selection SelectedValue { get; set; }


        public int recAmount { get; set; }
        public bool unlimited { get; set; }
        [DataType(DataType.Date)]
        public DateTime? RecEndDate { get; set; }

        public int[] SelectedRecurrID { get; set; }
        public IList<SelectListItem> recurrDropList { get; set; }

        public enum Selection
        {
            Option1,
            Option2,
            Option3

        }

    }
}