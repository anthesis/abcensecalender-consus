﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TutorialCS.Models;

/// <summary>
/// Summary description for Employee
/// </summary>
public class Employee
{
    public int RowIndex { get; set; }
    public string EmployeeID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Department { get; set; }
    public string DepartmentID { get; set; }
    public string dispname { get; set; }

    private IList<Project> ProjectList = new List<Project>();

    private IList<AbsenceDay> _eventList = new List<AbsenceDay>();

    public Employee(string id, string firstname, string lastname, string dep,string DepId )
    {
        EmployeeID = id;
        FirstName = firstname;
        LastName = lastname;
        Department = dep;
        DepartmentID = DepId;
    }
    public Employee(string id, string firstname, string lastname, string dep, string DepId, IList<AbsenceDay> eventlist)
    {
        EmployeeID = id;
        FirstName = firstname;
        LastName = lastname;
        Department = dep;
        DepartmentID = DepId;
        Events = eventlist;
    }

    public Employee()
    {
    }


    public IList<Project> Projects
    {
        get
        {
            return ProjectList;
        }
        set
        {
            ProjectList = value;
        }
    }

    public IList<AbsenceDay> Events
    {
        get
        {
            return _eventList;
        }
        set
        {
            _eventList = value;
        }
    }
}