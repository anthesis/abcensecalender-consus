﻿using DayPilot.Web.Mvc.Recurrence;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TutorialCS.Controllers;
using TutorialCS.Models;
using TutorialCS.Util;

namespace TutorialCS.DataAccess
{
    public class DAL_BYD
    {
        /// <summary>
        /// Ws For getting all Employees
        /// </summary>
        /// <returns>Emoployee List </returns>
        public int getAllEmployees()
        {
            GetEmployee.service service = createEmployeeService();

            GetEmployee.PBO_EmployeeRequestCreateRequestMessage_sync request = new GetEmployee.PBO_EmployeeRequestCreateRequestMessage_sync();
            request.BasicMessageHeader = new GetEmployee.BusinessDocumentBasicMessageHeader();
            request.PBO_EmployeeRequest = new GetEmployee.PBO_EmployeeRequestCreateRequest();

            GetEmployee.PBO_EmployeeRequestCreateConfirmationMessage_sync response = new GetEmployee.PBO_EmployeeRequestCreateConfirmationMessage_sync();
            try
            {
                response = service.Create(request);
            }
            catch (WebException e)
            {
                var test = e;

                if (e.Message.Contains("401"))
                {
                    HttpContext.Current.Session["ERROR"] = "Sie besitzen nicht die Rechte dazu"; 
                    return 1;

                }
            }




            Util.Util util = new Util.Util();
            if (response.PBO_EmployeeRequest != null)
            {
                string UUID = response.PBO_EmployeeRequest.SAP_UUID;
                var empList = emlpoyeeFromUUID(UUID);
                HttpContext.Current.Session["emplist"] = empList;
                //HttpContext.Current.Session["emplist"]= util.fillEmployeeListWithProject(HttpContext.Current.Session["emplist"] as List<Employee>);
                HttpContext.Current.Session["ProjList"] = util.getProjectList();
                empList = filterEmplist(HttpContext.Current.Session["empForProject"] as List<Employee>);
                HttpContext.Current.Session["emplist"] = empList;
                HttpContext.Current.Session["DepList"] = util.getDeparments(HttpContext.Current.Session["emplist"] as List<Employee>);
                //HttpContext.Current.Session["ProjList"] = util.getProjectList(HttpContext.Current.Session["empForProject"] as List<Employee>);

                empList = fillWithSAPevents(HttpContext.Current.Session["emplist"] as List<Employee>);
                HttpContext.Current.Session["emplist"] = empList;
                HttpContext.Current.Session["emplist"] = filWithProject(HttpContext.Current.Session["ProjList"] as List<Project>);
                return 2;
            }
            else
            {
                HttpContext.Current.Session["ERROR"] = "Fehler beim Webservice Aufruf. Bitte erneut versuchen.";
                return 1;
            }
        }
        private List<Employee> filWithProject(List<Project> prolist)
        {
            List<Employee> empList = HttpContext.Current.Session["emplist"] as List<Employee>;
            foreach (var item in empList)
            {
                item.Projects = prolist;
            }
            return empList;
        }
        private List<Employee> filterEmplist(List<Employee> empList)
        {
           List<Employee> allEmployees = new List<Employee>();
            List<Employee> filledList = HttpContext.Current.Session["emplist"] as List<Employee>;

            if (SelectedUser.seeAll)
            {
                allEmployees = new List<Employee>(filledList);
                return allEmployees;
            }
            if (SelectedUser.seeDep)
            {
                foreach (var item in empList)
                {

                    if (item.Department != null)
                    {
                        string[] buffer = item.Department.Split(' ');
                        string depName = buffer[0];
                        if (SelectedUser.SelectedDepName == depName)
                        {
                            foreach (var emp in filledList)
                            {
                                if (item.EmployeeID == emp.EmployeeID)
                                {
                                    allEmployees.Add(new Employee(item.EmployeeID, item.FirstName, item.LastName, depName, item.DepartmentID, emp.Events));
                                }

                            }
                        }
                    }
                }
                return allEmployees;
            }
            if (SelectedUser.seeAll ==false && SelectedUser.seeDep == false)
            { 
                foreach (var item in empList)
                {
                    string selUser = SelectedUser.SelectedUserName.TrimStart('0');
                    if (selUser == item.EmployeeID)
                    {
                        foreach (var emp in filledList)
                        {
                            if (item.EmployeeID == emp.EmployeeID)
                            {
                                allEmployees.Add(new Employee(item.EmployeeID, item.FirstName, item.LastName, item.Department, item.DepartmentID, emp.Events));
                            }

                        }   
                    }
                }
            }
            return allEmployees;

        }

        private List<Employee> emlpoyeeFromUUID (string UUID)
        {
            List<Employee> allEmployees = new List<Employee>();
            List<Employee> empForProject = new List<Employee>();
            GetEmployee.service service = createEmployeeService();

            GetEmployee.PBO_EmployeeRequestReadByIDQueryMessage_sync request = new GetEmployee.PBO_EmployeeRequestReadByIDQueryMessage_sync();
            request.PBO_EmployeeRequest = new GetEmployee.PBO_EmployeeRequestReadByIDQuery();
            request.PBO_EmployeeRequest.SAP_UUID = UUID;

            GetEmployee.PBO_EmployeeRequestReadByIDResponseMessage_sync response = new GetEmployee.PBO_EmployeeRequestReadByIDResponseMessage_sync();

            response = service.Read(request);


            string User = HttpContext.Current.Session["Username"].ToString();
            User = User.ToUpper();

            foreach (var item in response.PBO_EmployeeRequest.Employee)
            {
                if (item.Username==User)
                {
                    SelectedUser.SelectedUserName= item.EmployeeID;
                    SelectedUser.seeAll = item.ACCompanyPermission;
                    SelectedUser.seeDep = item.ACDepartmentPermission;
                    SelectedUser.SelectedDepID = item.DepartmentID;
                    if (item.DepartmentName != null)
                    {
                        string[] buffer = item.DepartmentName.Split(' ');
                        string depName = buffer[0];
                        SelectedUser.SelectedDepName = depName;
                    }

                }
            }
            if (User == "ANTHESIS")
            {
                SelectedUser.seeAll = true;
            }
            if (SelectedUser.SelectedUserName.TrimStart('0') == "7000000")
            {
                SelectedUser.seeDep = true;
                SelectedUser.SelectedDepName = "Medizincontrolling";
            }

            if (SelectedUser.seeAll)
            {
                foreach (var item in response.PBO_EmployeeRequest.Employee)
                {
                    string empID = item.EmployeeID.TrimStart('0');
                    allEmployees.Add(new Employee(empID, item.GivenName, item.FamiliyName, item.DepartmentName, item.DepartmentID));
                }
            }
            if (SelectedUser.seeDep)
            {
                foreach (var item in response.PBO_EmployeeRequest.Employee)
                {
                    if (item.DepartmentName!= null)
                    {

                    
                    string[] buffer = item.DepartmentName.Split(' ');
                    string depName = buffer[0];

                    if (SelectedUser.SelectedDepName == depName)
                    {
                        string empID = item.EmployeeID.TrimStart('0');
                        allEmployees.Add(new Employee(empID, item.GivenName, item.FamiliyName, depName, item.DepartmentID));
                    }
                    if (SelectedUser.SelectedUserName == "J.GOERING")
                    {
                        if (SelectedUser.SelectedDepName == "Medizincontrolling")
                        {
                            string empID = item.EmployeeID.TrimStart('0');
                            allEmployees.Add(new Employee(empID, item.GivenName, item.FamiliyName, depName, item.DepartmentID));
                        }
                    }

                }
                }

            }
            else
            {
                foreach (var item in response.PBO_EmployeeRequest.Employee)
                {
                    if (SelectedUser.SelectedUserName == item.EmployeeID)
                    {
                        string empID = item.EmployeeID.TrimStart('0');
                        allEmployees.Add(new Employee(empID, item.GivenName, item.FamiliyName, item.DepartmentName, item.DepartmentID));
                    }

                }
            }
            foreach (var item in response.PBO_EmployeeRequest.Employee)
            {
                string empID = item.EmployeeID.TrimStart('0');
                empForProject.Add(new Employee(empID, item.GivenName, item.FamiliyName, item.DepartmentName, item.DepartmentID));

            }

            HttpContext.Current.Session["empForProject"] = empForProject;


            var test = allEmployees;

           return fillEmployeList(allEmployees);

        } 

        public List<Employee> fillEmployeList (List<Employee> empList)
        {
            int empListLength = empList.Count();
            var retList= new List<Employee>();

            QueryEmployeeTimeIn.service service = createQueryEmplyoyeeService();
            QueryEmployeeTimeIn.ActualEmployeeTimeByElementsQueryMessage_sync request = new QueryEmployeeTimeIn.ActualEmployeeTimeByElementsQueryMessage_sync();

            request.ActualEmployeeTimeSelectionByElements = new QueryEmployeeTimeIn.ActualEmployeeTimeByElementsQuery();

            request.ActualEmployeeTimeSelectionByElements.SelectionByEmployeeID = new QueryEmployeeTimeIn.ActualEmployeeTimeByElementsQuerySelectionByEmployeeID[empListLength];
            request.ProcessingConditions = new QueryEmployeeTimeIn.QueryProcessingConditions();
            request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;

            QueryEmployeeTimeIn.ActualEmployeeTimeByElementsResponseMessage_sync response = new QueryEmployeeTimeIn.ActualEmployeeTimeByElementsResponseMessage_sync();

            for (int i = 0; i < empList.Count; i++)
            {

                request.ActualEmployeeTimeSelectionByElements.SelectionByEmployeeID[i] = new QueryEmployeeTimeIn.ActualEmployeeTimeByElementsQuerySelectionByEmployeeID();
                request.ActualEmployeeTimeSelectionByElements.SelectionByEmployeeID[i].IntervalBoundaryTypeCode = "1";
                request.ActualEmployeeTimeSelectionByElements.SelectionByEmployeeID[i].InclusionExclusionCode = "I";
                request.ActualEmployeeTimeSelectionByElements.SelectionByEmployeeID[i].LowerBoundaryEmployeeID = new QueryEmployeeTimeIn.EmployeeID();
                request.ActualEmployeeTimeSelectionByElements.SelectionByEmployeeID[i].LowerBoundaryEmployeeID.Value = empList[i].EmployeeID;
            }

            

            response = service.FindActualEmployeeTimeByElements(request);


            Util.Util trasnformData = new Util.Util();
            //retList =  trasnformData.getEmployeesWithTimes(response, empList);
            retList = empList;
            retList = trasnformData.makeUniqueEmployees(retList);
            retList = trasnformData.FillWithSAPEmployeeTimes(retList, response); 

            return retList;
        }

        public List<Employee> fillWithSAPevents(List<Employee> empList)
        {
            Util.Util util = new Util.Util();
           

            EmployeePlanning.service service = createEmployeePlanningService();
            foreach (var employee in empList)
            {
                EmployeePlanning.PBO_EmployeeTimesQueryByElementsSimpleByRequestMessage_sync request = new EmployeePlanning.PBO_EmployeeTimesQueryByElementsSimpleByRequestMessage_sync();
                EmployeePlanning.PBO_EmployeeTimesQueryByElementsSimpleByConfirmationMessage_sync response = new EmployeePlanning.PBO_EmployeeTimesQueryByElementsSimpleByConfirmationMessage_sync();
                request.PBO_EmployeeTimesSimpleSelectionBy = new EmployeePlanning.PBO_EmployeeTimesQueryByElementsSimpleByRequest();
                request.PBO_EmployeeTimesSimpleSelectionBy.SelectionByEmployeeID = new EmployeePlanning.PBO_EmployeeTimesQueryByElementsSimpleByRequestSelectionByEmployeeID[1];
                request.PBO_EmployeeTimesSimpleSelectionBy.SelectionByEmployeeID[0] = new EmployeePlanning.PBO_EmployeeTimesQueryByElementsSimpleByRequestSelectionByEmployeeID();
                request.PBO_EmployeeTimesSimpleSelectionBy.SelectionByEmployeeID[0].InclusionExclusionCode = "I";
                request.PBO_EmployeeTimesSimpleSelectionBy.SelectionByEmployeeID[0].IntervalBoundaryTypeCode = "1";
                request.PBO_EmployeeTimesSimpleSelectionBy.SelectionByEmployeeID[0].LowerBoundaryEmployeeID  = new EmployeePlanning.EmployeeID();
                request.PBO_EmployeeTimesSimpleSelectionBy.SelectionByEmployeeID[0].LowerBoundaryEmployeeID.Value = employee.EmployeeID;
                request.ProcessingConditions = new EmployeePlanning.QueryProcessingConditions();
                request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;

                response = service.QueryByElements(request);
                if (response.PBO_EmployeeTimes != null)
                {
                    foreach (var item in response.PBO_EmployeeTimes)
                    {
                        string displayName = util.getProjectNameFromProjectID(item.ProjectID.Value);

                        if (item.BlockingTimeFrame.StartTimePoint.DateTime !=null && item.BlockingTimeFrame.EndTimePoint.DateTime != null)
                        {
                            DateTime start = Convert.ToDateTime(item.BlockingTimeFrame.StartTimePoint.DateTime.Value);
                            DateTime end = Convert.ToDateTime(item.BlockingTimeFrame.EndTimePoint.DateTime.Value);
                            int RecTimes = item.ReccuringEndDateDuration;
                            if (item.UnlimitedRecurringIndicator || (RecTimes>0 && item.ReccuringEndDate!= null)) 
                            {
                                DateTime recEnd = Convert.ToDateTime(item.ReccuringEndDate);
                                string recType = "";
                                RecurrenceRule rule = null;                                
                                switch (item.RecurringType)
                                {
                                    case EmployeePlanning.CL_RecurringTypesCode.NONE:
                                        recType = null;
                                        break;
                                    case EmployeePlanning.CL_RecurringTypesCode.DAILY:
                                        recType = "Täglich";
                                        break;
                                    case EmployeePlanning.CL_RecurringTypesCode.WEEKLY:
                                        recType = "Wöchentlich";
                                        break;
                                    case EmployeePlanning.CL_RecurringTypesCode.BIWEEKLY:
                                        recType = "Zwei-Wöchentlich";
                                        break;
                                    case EmployeePlanning.CL_RecurringTypesCode.MONTHLY:
                                        recType = "Monatlich";
                                        break;
                                    case EmployeePlanning.CL_RecurringTypesCode.YEARLY:
                                        recType = "Jährlich";
                                        break;
                                    default:
                                        break;
                                }
                                if (item.UnlimitedRecurringIndicator)
                                {
                                     rule = util.createRecRuleUnlimited(recType, start, item.SAP_UUID);
                                }
                                else
                                {
                                    if (RecTimes >0)
                                    {
                                       rule = util.createRecRule(recType, start, RecTimes, item.SAP_UUID);
                                    }
                                    else
                                    {
                                        rule = util.createRecRule(recType, start, recEnd, item.SAP_UUID);
                                    }
                                    
                                }
                                employee.Events.Add(new AbsenceDay(item.SAP_UUID, displayName, start, end, item.SAP_UUID,rule));
                            }
                            else
                            {   
                              employee.Events.Add(new AbsenceDay(item.SAP_UUID, displayName, start, end, item.SAP_UUID));      
                            }
                            
                        }

                    }
                }

           }
            return empList;
           

        }

        public void UpdateSAPevents(string uuid, DateTime start, DateTime end, string empID)
        {
            EmployeePlanning.service service = createEmployeePlanningService();
            EmployeePlanning.PBO_EmployeeTimesUpdateRequestMessage_sync request = new EmployeePlanning.PBO_EmployeeTimesUpdateRequestMessage_sync();
            request.PBO_EmployeeTimes = new EmployeePlanning.PBO_EmployeeTimesUpdateRequest();

            request.PBO_EmployeeTimes.EmployeeID = new EmployeePlanning.EmployeeID();
            request.PBO_EmployeeTimes.EmployeeID.Value = empID;

            request.PBO_EmployeeTimes.SAP_UUID = uuid;

            request.PBO_EmployeeTimes.BlockingTimeFrame = new EmployeePlanning.TimePointPeriod();

            request.PBO_EmployeeTimes.BlockingTimeFrame.IntervalBoundaryTypeCode = "2";

            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint = new EmployeePlanning.TimePoint();
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.TypeCode = "2";
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime = new EmployeePlanning.DateTime();
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime.Value = Convert.ToDateTime(start);


            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint = new EmployeePlanning.TimePoint();
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.TypeCode = "2";
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime = new EmployeePlanning.DateTime();
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime.Value = Convert.ToDateTime(end);


            service.Update(request);

        }

        public void DeleteSAPTimes(string uuid)
        {
            EmployeePlanning.service service = createEmployeePlanningService();
            EmployeePlanning.PBO_EmployeeTimesDeletePlanningTimeDeletePlanningTimeRequest_syncMessage request = new EmployeePlanning.PBO_EmployeeTimesDeletePlanningTimeDeletePlanningTimeRequest_syncMessage();
            request.PBO_EmployeeTimes = new EmployeePlanning.PBO_EmployeeTimesDeletePlanningTimeDeletePlanningTimeRequest();
            request.PBO_EmployeeTimes.SAP_UUID = uuid;
            service.DeletePlanningTime(request);

        }

        public string SaveTimes(string start, string end, string projectID , string empID)
        {
            string SAP_UUID = "";

            EmployeePlanning.service service = createEmployeePlanningService();
            EmployeePlanning.PBO_EmployeeTimesCreateRequestMessage_sync request = new EmployeePlanning.PBO_EmployeeTimesCreateRequestMessage_sync();

            EmployeePlanning.PBO_EmployeeTimesCreateConfirmationMessage_sync response = new EmployeePlanning.PBO_EmployeeTimesCreateConfirmationMessage_sync();
            request.PBO_EmployeeTimes = new EmployeePlanning.PBO_EmployeeTimesCreateRequest();    

                request.PBO_EmployeeTimes.EmployeeID = new EmployeePlanning.EmployeeID();
                request.PBO_EmployeeTimes.EmployeeID.Value = empID;

                request.PBO_EmployeeTimes.BlockingTimeFrame = new EmployeePlanning.TimePointPeriod();
                request.PBO_EmployeeTimes.BlockingTimeFrame.IntervalBoundaryTypeCode = "2";

                request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint = new EmployeePlanning.TimePoint();
                request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.TypeCode = "2";
                request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime = new EmployeePlanning.DateTime();
                request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime.Value = Convert.ToDateTime(start);
                 
                           
                request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint = new EmployeePlanning.TimePoint();
                request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.TypeCode = "2";
                request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime = new EmployeePlanning.DateTime();
                request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime.Value = Convert.ToDateTime(end);

                request.PBO_EmployeeTimes.ProjectID = new EmployeePlanning.ProjectID();
                request.PBO_EmployeeTimes.ProjectID.Value = projectID;
                request.PBO_EmployeeTimes.RecurringTypeSpecified = true;
                request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.NONE;


            response = service.Create(request);

            SAP_UUID = response.PBO_EmployeeTimes.SAP_UUID;
            return SAP_UUID;
         }
        public string SaveTimes(string start, string end, string projectID, string empID, string recType, DateTime recEndDate, Boolean unlimited)
        {
            string SAP_UUID = "";

            EmployeePlanning.service service = createEmployeePlanningService();
            EmployeePlanning.PBO_EmployeeTimesCreateRequestMessage_sync request = new EmployeePlanning.PBO_EmployeeTimesCreateRequestMessage_sync();

            EmployeePlanning.PBO_EmployeeTimesCreateConfirmationMessage_sync response = new EmployeePlanning.PBO_EmployeeTimesCreateConfirmationMessage_sync();
            request.PBO_EmployeeTimes = new EmployeePlanning.PBO_EmployeeTimesCreateRequest();

            request.PBO_EmployeeTimes.EmployeeID = new EmployeePlanning.EmployeeID();
            request.PBO_EmployeeTimes.EmployeeID.Value = empID;

            request.PBO_EmployeeTimes.BlockingTimeFrame = new EmployeePlanning.TimePointPeriod();
            request.PBO_EmployeeTimes.BlockingTimeFrame.IntervalBoundaryTypeCode = "2";

            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint = new EmployeePlanning.TimePoint();
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.TypeCode = "2";
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime = new EmployeePlanning.DateTime();
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime.Value = Convert.ToDateTime(start);


            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint = new EmployeePlanning.TimePoint();
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.TypeCode = "2";
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime = new EmployeePlanning.DateTime();
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime.Value = Convert.ToDateTime(end);

            request.PBO_EmployeeTimes.ProjectID = new EmployeePlanning.ProjectID();
            request.PBO_EmployeeTimes.ProjectID.Value = projectID;
            request.PBO_EmployeeTimes.ReccuringEndDateSpecified = true;
            request.PBO_EmployeeTimes.ReccuringEndDate = new DateTime(recEndDate.Year, recEndDate.Month, recEndDate.Day);
            request.PBO_EmployeeTimes.RecurringTypeSpecified = true;


            switch (recType)
            {
                case "Täglich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.DAILY;
                    
                    break;
                case "Wöchentlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.WEEKLY;
                    break;
                case "Zwei-Wöchentlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.BIWEEKLY;
                    break;
                case "Monatlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.MONTHLY;
                    break;
                case "Jährlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.YEARLY;
                    break;
                default:
                    break;
            }

            response = service.Create(request);

            SAP_UUID = response.PBO_EmployeeTimes.SAP_UUID;
            return SAP_UUID;
        }
        public string SaveTimes(string start, string end, string projectID, string empID, string recType, int recTimes)
        {
            string SAP_UUID = "";

            EmployeePlanning.service service = createEmployeePlanningService();
            EmployeePlanning.PBO_EmployeeTimesCreateRequestMessage_sync request = new EmployeePlanning.PBO_EmployeeTimesCreateRequestMessage_sync();

            EmployeePlanning.PBO_EmployeeTimesCreateConfirmationMessage_sync response = new EmployeePlanning.PBO_EmployeeTimesCreateConfirmationMessage_sync();
            request.PBO_EmployeeTimes = new EmployeePlanning.PBO_EmployeeTimesCreateRequest();

            request.PBO_EmployeeTimes.EmployeeID = new EmployeePlanning.EmployeeID();
            request.PBO_EmployeeTimes.EmployeeID.Value = empID;

            request.PBO_EmployeeTimes.BlockingTimeFrame = new EmployeePlanning.TimePointPeriod();
            request.PBO_EmployeeTimes.BlockingTimeFrame.IntervalBoundaryTypeCode = "2";

            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint = new EmployeePlanning.TimePoint();
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.TypeCode = "2";
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime = new EmployeePlanning.DateTime();
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime.Value = Convert.ToDateTime(start);


            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint = new EmployeePlanning.TimePoint();
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.TypeCode = "2";
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime = new EmployeePlanning.DateTime();
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime.Value = Convert.ToDateTime(end);

            request.PBO_EmployeeTimes.ProjectID = new EmployeePlanning.ProjectID();
            request.PBO_EmployeeTimes.ProjectID.Value = projectID;
            request.PBO_EmployeeTimes.ReccuringEndDateDurationSpecified = true;
            request.PBO_EmployeeTimes.ReccuringEndDateDuration = recTimes;
            request.PBO_EmployeeTimes.RecurringTypeSpecified = true;


            switch (recType)
            {
                case "Täglich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.DAILY;

                    break;
                case "Wöchentlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.WEEKLY;
                    break;
                case "Zwei-Wöchentlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.BIWEEKLY;
                    break;
                case "Monatlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.MONTHLY;
                    break;
                case "Jährlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.YEARLY;
                    break;
                default:
                    break;
            }

            response = service.Create(request);

            SAP_UUID = response.PBO_EmployeeTimes.SAP_UUID;
            return SAP_UUID;
        }
        public string SaveTimes(string start, string end, string projectID, string empID, string recType, Boolean unlimited)
        {
            string SAP_UUID = "";

            EmployeePlanning.service service = createEmployeePlanningService();
            EmployeePlanning.PBO_EmployeeTimesCreateRequestMessage_sync request = new EmployeePlanning.PBO_EmployeeTimesCreateRequestMessage_sync();

            EmployeePlanning.PBO_EmployeeTimesCreateConfirmationMessage_sync response = new EmployeePlanning.PBO_EmployeeTimesCreateConfirmationMessage_sync();
            request.PBO_EmployeeTimes = new EmployeePlanning.PBO_EmployeeTimesCreateRequest();

            request.PBO_EmployeeTimes.EmployeeID = new EmployeePlanning.EmployeeID();
            request.PBO_EmployeeTimes.EmployeeID.Value = empID;

            request.PBO_EmployeeTimes.BlockingTimeFrame = new EmployeePlanning.TimePointPeriod();
            request.PBO_EmployeeTimes.BlockingTimeFrame.IntervalBoundaryTypeCode = "2";

            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint = new EmployeePlanning.TimePoint();
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.TypeCode = "2";
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime = new EmployeePlanning.DateTime();
            request.PBO_EmployeeTimes.BlockingTimeFrame.StartTimePoint.DateTime.Value = Convert.ToDateTime(start);


            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint = new EmployeePlanning.TimePoint();
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.TypeCode = "2";
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime = new EmployeePlanning.DateTime();
            request.PBO_EmployeeTimes.BlockingTimeFrame.EndTimePoint.DateTime.Value = Convert.ToDateTime(end);

            request.PBO_EmployeeTimes.ProjectID = new EmployeePlanning.ProjectID();
            request.PBO_EmployeeTimes.ProjectID.Value = projectID;

            request.PBO_EmployeeTimes.RecurringTypeSpecified = true;

            switch (recType)
            {
                case "Täglich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.DAILY;
                    break;
                case "Wöchentlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.WEEKLY;
                    break;
                case "Zwei-Wöchentlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.BIWEEKLY;
                    break;
                case "Monatlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.MONTHLY;
                    break;
                case "Jährlich":
                    request.PBO_EmployeeTimes.RecurringType = EmployeePlanning.CL_RecurringTypesCode.YEARLY;
                    break;
                default:
                    break;
            }
            request.PBO_EmployeeTimes.UnlimitedRecurringIndicatorSpecified = true;
            request.PBO_EmployeeTimes.UnlimitedRecurringIndicator = unlimited;

            response = service.Create(request);

            SAP_UUID = response.PBO_EmployeeTimes.SAP_UUID;
            return SAP_UUID;
        }


        public List<Project> getProjectList()
        {
            List<Project> retList = new List<Project>();
            QueryProjectIn.service service = createQueryProjectService();
            QueryProjectIn.ProjectByElementsQueryMessage_sync request = new QueryProjectIn.ProjectByElementsQueryMessage_sync();
            request.ProjectSelectionByElements = new QueryProjectIn.ProjectByElementsQuerySelectionByElements();

            request.ProjectSelectionByElements.SelectionByProjectID = new QueryProjectIn.ProjectSelectionByProjectId[1];
            request.ProjectSelectionByElements.SelectionByProjectID[0] = new QueryProjectIn.ProjectSelectionByProjectId();
            request.ProjectSelectionByElements.SelectionByProjectID[0].InclusionExclusionCode = "I";
            request.ProjectSelectionByElements.SelectionByProjectID[0].IntervalBoundaryTypeCode = "1";
            request.ProjectSelectionByElements.SelectionByProjectID[0].LowerBoundaryProjectID = new QueryProjectIn.ProjectID();
            request.ProjectSelectionByElements.SelectionByProjectID[0].LowerBoundaryProjectID.Value = "*";

            request.ProcessingConditions = new QueryProjectIn.QueryProcessingConditions();
            request.ProcessingConditions.QueryHitsUnlimitedIndicator = true;

            QueryProjectIn.ProjectByElementsResponseMessage_sync response = new QueryProjectIn.ProjectByElementsResponseMessage_sync();

            response = service.FindProjectByElements(request);

            foreach (var proj in response.ProjectQueryResponse)
            {
                if (proj.ProjectStatus.ProjectLifeCycleStatusCode == QueryProjectIn.ProjectLifeCycleStatusCode.Item3)
                {
                    retList.Add(new Project(proj.ProjectTask[0].TaskName[0].Name.Value, proj.ProjectID.Value));
                }
            }
            return retList;
        }

        private QueryProjectIn.service createQueryProjectService()
        {
            QueryProjectIn.service service = new QueryProjectIn.service();
            service.Url = ConnectionConfig.QueryProject;
            service.Credentials = new NetworkCredential(WebConfigurationManager.AppSettings["Username"], WebConfigurationManager.AppSettings["Password"]);
            //if (!HttpContext.Current.Request.Url.Host.Contains("localhost"))
            //    service.Proxy = new WebProxy("http://winproxy.schlund.de:3128");
            return service;

        }

        private EmployeePlanning.service createEmployeePlanningService()
        {
            EmployeePlanning.service service = new EmployeePlanning.service();
            service.Url = ConnectionConfig.EmployeePlanning;
            service.Credentials = new NetworkCredential(HttpContext.Current.Session["Username"].ToString(), HttpContext.Current.Session["Password"].ToString());
            //if (!HttpContext.Current.Request.Url.Host.Contains("localhost"))
            //    service.Proxy = new WebProxy("http://winproxy.schlund.de:3128");
            return service;

        }
        private ProjectRequestIn.service createProjectRequestInService()
        {
            ProjectRequestIn.service service = new ProjectRequestIn.service();

            service.Url = ConnectionConfig.ProjectUrl;
            service.Credentials = new NetworkCredential(HttpContext.Current.Session["Username"].ToString(), HttpContext.Current.Session["Password"].ToString());


            //Activate this proxy for productiv usage on 1&1 IIS Server, disable for local tests
            //if (!HttpContext.Current.Request.Url.Host.Contains("localhost"))
            //    service.Proxy = new WebProxy("http://winproxy.schlund.de:3128");
            return service;

        }


        private GetEmployee.service createEmployeeService()
        {
            GetEmployee.service service = new GetEmployee.service();

            service.Url = ConnectionConfig.EmployeeUrl;
            service.Credentials = new NetworkCredential(HttpContext.Current.Session["Username"].ToString(), HttpContext.Current.Session["Password"].ToString());

            //Activate this proxy for productiv usage on 1&1 IIS Server, disable for local tests
            //if (!HttpContext.Current.Request.Url.Host.Contains("localhost"))
            //    service.Proxy = new WebProxy("http://winproxy.schlund.de:3128");
            return service;

        }
        private QueryEmployeeTimeIn.service createQueryEmplyoyeeService()
        {
            QueryEmployeeTimeIn.service service = new QueryEmployeeTimeIn.service();
            service.Url = ConnectionConfig.QueryUrl;
            service.Credentials = new NetworkCredential(WebConfigurationManager.AppSettings["Username"], WebConfigurationManager.AppSettings["Password"]);

            //Activate this proxy for productiv usage on 1&1 IIS Server, disable for local tests
            //if (!HttpContext.Current.Request.Url.Host.Contains("localhost"))
            //    service.Proxy = new WebProxy("http://winproxy.schlund.de:3128");
            return service;
        }

    }
}