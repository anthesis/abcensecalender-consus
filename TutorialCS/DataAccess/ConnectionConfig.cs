﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace TutorialCS.DataAccess
{
    public static class ConnectionConfig
    {
        public const string TEST = "TEST";
        public const string PROD = "PROD";
        /// <summary>
        /// Test or PROD
        /// </summary>
        public static string Enviroment
        {
            get
            {
                return ConfigurationManager.AppSettings["Enviroment"];
            }
        }
        public static string EmployeeUrl
        {
            get
            {
                switch(Enviroment){
                    case TEST: return ConfigurationManager.AppSettings["emplyoeeUrl_TEST"];
                    case PROD: return ConfigurationManager.AppSettings["emplyoeeUrl_PROD"];
                    default: throw new Exception("Enviroment not Supported");
                }
            }
        }
        public static string QueryUrl
        {
            get
            {
                switch (Enviroment)
                {
                    case TEST: return ConfigurationManager.AppSettings["QueryUrl_TEST"];
                    case PROD: return ConfigurationManager.AppSettings["QueryUrl_PROD"];
                    default: throw new Exception("Enviroment not Supported");
                }
            }
        }
        public static string ProjectUrl
        {
            get
            {
                switch (Enviroment)
                {
                    case TEST: return ConfigurationManager.AppSettings["ProjectUrl_TEST"];
                    case PROD: return ConfigurationManager.AppSettings["ProjectUrl_PROD"];
                    default: throw new Exception("Enviroment not Supported");
                }
            }
        }
        public static string QueryProject
        {
            get
            {
                switch (Enviroment)
                {
                    case TEST: return ConfigurationManager.AppSettings["QueryProject_TEST"];
                    case PROD: return ConfigurationManager.AppSettings["QueryProject_PROD"];
                    default: throw new Exception("Enviroment not Supported");
                }
            }
        }
        public static string EmployeePlanning
        {
            get
            {
                switch (Enviroment)
                {
                    case TEST: return ConfigurationManager.AppSettings["EmployeePlanning_TEST"];
                    case PROD: return ConfigurationManager.AppSettings["EmployeePlanning_PROD"];
                    default: throw new Exception("Enviroment not Supported");
                }
            }
        }
    }
}